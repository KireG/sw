<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
    <script type="text/javascript" src="javascript.js"></script>
    <link href="../../Public/style.css" rel="stylesheet">

</head>
<body>

<div class="container main">
    <div class="form-popup" id="openp" style="display:none";>
      <div class="modal-body">
        <h4>Please, submit required data</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary close" id="closep">Exit</button>
      </div>
        
    </div>
    <div class="form-popup" id="openn" style="display:none";>
      <div class="modal-body">
        <h4>Please, provide the data of indicated type</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary close" id="closen">Exit</button>
      </div>
        
    </div>
    <div class="row headdiv">
        <div class="col md-3 titlediv"><h3><?php echo $title ?></h3></div>
        <div class="col md-3 offset-md-6 buttdiv">
            <a href="<?php echo $b1link?>"><button type="submit" class="btn btn-light" id="<?php echo $btn1id ?>" form="<?php echo $form ?>"><?php echo $b1?></button></a> 
            <a href="<?php echo $b2link?>"><button type="button" class="btn btn-light" id="<?php echo $btn2id ?>"><?php echo $b2?></button></a>  
        </div>
    </div>
    