<div class="formpart " id="formpart3">
    <div class="row">
        <div class="col-md-8 formpartspec">
            <div class="row spec">
                <div class="col-md-3">
                    <div class="formnam">Height (CM)</div>
                </div>
                <div class="col-md-6">
                    <input  type="text" id="height" name="height">
                </div>
            </div>
            <div class="row spec">
                <div class="col-md-3">
                    <div class="formnam">Width (CM)</div>
                </div>
                <div class="col-md-6">
                    <input  type="text" id="width" name="width">
                </div>
            </div>
            <div class="row spec">
                <div class="col-md-3">
                    <div class="formnam">Length (CM)</div>
                </div>
                <div class="col-md-6">
                    <input  type="text" id="length" name="length">
                </div>
            </div>
            <div class="row spec">
                <div class="col-md-12">
                    Please provide dimensions"
                </div>
        </div>
     </div>  
    </div>
</div>