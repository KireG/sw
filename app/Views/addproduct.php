<?php

use KG\Models\Type;

include_once 'autoload.php';

// Seting variables
$title="Product Add";
$b1="Save";
$b2="Cancel";
$form ="newel";
$b1link="";
$b2link="productlist.php";
$btn1id="new_product";
$btn2id="";
//Creating
$typ = new Type;
$types =$typ->getobject(); 

include 'Layouts/header.php';
?>
<div id="object_type"></div>
<div class="row" style="height:82vh;padding-top:20px">
<div class="col-md-6">
<form action=" " method="POST" id="newel">
<div class="formpart" id="formpart">
    <div class="row">
        <div class="col-md-2">
        <div class="formnam">SKU</div>
        </div>
        <div class="col-md-6">
        <input class="forminp" type="text" id="sku" name="SKU">
        </div>
    </div>
</div>
<div class="formpart" id="formpart">
    <div class="row">
        <div class="col-md-2">
        <div class="formnam">Name</div>
        </div>
        <div class="col-md-6">
        <input class="forminp" type="text" id="name" name="name">
        </div>
    </div>
</div>
<div class="formpart" id="formpart">
    <div class="row">
        <div class="col-md-2">
        <div class="formnam">Price</div>
        </div>
        <div class="col-md-6">
        <input class="forminp" type="text" id="price" name="price">
        </div>
    </div>
</div>
<div class="formpart" id="formpart">
    <div class="row">
        <div class="col-md-2">
        <div class="formnam">Type</div>
        </div>
        <div class="col-md-6">
            <select class="formsel " style="outline: none;" name="type_id" id="valsel">
                <option value=" " selected >Select option</option>
                <?php foreach ($types as $type) {
                    ?><option value="<?php echo $type['id']; ?>" id="val<?php echo $type['id'];?>"><?php echo $type['name']; ?></option>
                    <?php } ?>
            </select>
        </div>
    </div>
</div>
<div id="adddiv1" class="adddiv" style="display:none"  >
<?php include "layouts/type1.php"; ?>
</div>
<div id="adddiv2" class="adddiv" style="display:none" >
<?php include "layouts/type2.php"; ?>
</div>
<div id="adddiv3" class="adddiv" style="display:none" >
<?php include "layouts/type3.php"; ?>
</div>
</form>
</div>
</div>

<?php
include 'Layouts/footer.php';
?>