<?php

use KG\Models\Dvd;
use KG\Models\Furniture;
use KG\Models\Book;

include 'autoload.php';

// Seting variables
$title="Product List";
$b1="Add";
$b2="Mass Delete";
$b1link="addproduct.php";
$b2link="";
$btn2id="delbtn";

// Creating object

$dvds=(new Dvd)->getobject();
$books=(new Book)->getobject();
$furnitures=(new Furniture)->getobject();
$products=array_merge($dvds,$books,$furnitures);

include 'Layouts/header.php';
?>
<div class="row" style="min-height:82vh;padding-top:20px">

<?php foreach($products as $product){
 ?> <div class="col-md-3">
<div class="midrow">
    <div class="row">
        <div class="col-md-12">
            <div class="form-check">
                <input class="form-check-input" data-column="<?php echo $product['type_id'];?>" type="checkbox"  name="type" value="<?php echo $product['id'];?>" id="check<?php echo $product['id'];?>" >
            </div>
        </div>
    </div>
    <div class="row info-div">
        <div class="sep-div"><?php echo $product['SKU'];?></div>
        <div class="sep-div"><?php echo $product['name'];?></div>
        <div class="sep-div"><?php echo $product['price'];?>$</div>
        <div class="sep-div"><?php 
        if($product['type'] == "DVD"){?> 
            Size: <?php echo $product['mb'];?> MB <?php }else if($product['type'] == "Book"){?> Weight: <?php echo $product['weight'];?> KG <?php }else if($product['type'] == "Furniture"){?> 
            Dimensions: <?php echo $product['height'];?>x<?php echo $product['width'];?>x<?php echo $product['length']; } ?>
        </div>   
    </div>   
</div>
</div>

<?php }?>
</div>
<?php
include 'Layouts/footer.php';
?>

