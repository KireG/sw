<?php
namespace KG\Models;

abstract class Product  {

  protected $connect;

  public function __construct(){
      $this->connect= Database::connect($this); 
  }
  
 // geting data from database
  public function getobject(){
      $sql = $this->query();
      $statement=$this->connect->prepare($sql);
      $statement->execute();
      $resoult=$statement->fetchAll(\PDO::FETCH_ASSOC);
      return $resoult;
  } 

// deleting project
  public function delete($todel,$location){
      $table=$this->gettable();
      $todelete=array_map('intval',explode(',',$todel));
    foreach($todelete as $i){
      $sql = "DELETE FROM $table WHERE id=$i";
      $statement = $this->connect->prepare($sql);
      $statement->execute();
    }
     header($location);
  }
// adding project 
  public function add_product($data){
    
    if($_SERVER['REQUEST_METHOD'] == "POST") {
      $table=$this->gettable();
      $new_object = $this->get_object_data($data);
      
     $sql = sprintf(
       "INSERT INTO %s (%s) values (%s)",
       $table,
       implode(", ", array_keys($new_object)),
       ":" . implode(", :", array_keys($new_object))
   );

     $statement = $this->connect->prepare($sql);
     try{
       $statement->execute($new_object);
     }catch (\PDOException $e){
       echo "Error";
     }
  }   
 }

}
