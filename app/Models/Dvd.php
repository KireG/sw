<?php

namespace KG\Models;

use KG\Models\Product;
use KG\Models\Load;

class Dvd extends Product implements Load_interface , Loger_interface{
    
    public function query(){
        return $sql= "SELECT dvds.id,dvds.name,dvds.SKU,dvds.price,dvds.type_id,types.name as type,dvds.mb
        FROM dvds JOIN types ON dvds.type_id=types.id             
        ORDER BY dvds.name";    
    } 
    
    public function get_object_data($data){
        
       return  $new_object=[
          'name'=> $_POST['name'],
          'sku'=>$_POST['SKU'],
          'price'=>$_POST['price'],
          'type_id'=>$_POST['type_id'],
          'mb'=>$_POST['mb'],
      ];
   
    }
     public function gettable(){
        return $table="dvds";    
     }
     
     public function locationAfterAdd(){
        return $location="productlist";   
     }

}