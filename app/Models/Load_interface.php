<?php

namespace KG\Models;

interface Load_interface {
    public function query();
    public function get_object_data($data);
    public function gettable();
    public function locationAfterAdd();
}