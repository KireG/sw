<?php

namespace KG\Models;

use KG\Models\Product;
use KG\Models\Load;

class Furniture extends Product implements Load_interface, Loger_interface{
    
    public function query(){
        return $sql= "SELECT furnitures.id,furnitures.name,furnitures.SKU,furnitures.price,furnitures.type_id,types.name as type,furnitures.height,furnitures.width,furnitures.length
        FROM furnitures JOIN types ON furnitures.type_id=types.id             
        ORDER BY furnitures.name";    
    } 
   
    public function get_object_data($data){
        
       return  $new_object=[
          'name'=> $_POST['name'],
          'sku'=>$_POST['SKU'],
          'price'=>$_POST['price'],
          'type_id'=>$_POST['type_id'],
          'height'=>$_POST['height'],
          'width'=>$_POST['width'],
          'length'=>$_POST['length'],
      ];
   
    }
     public function gettable(){
        return $table="furnitures";    
     }

     public function locationAfterAdd(){
        return $location="productlist";   
     } 

}