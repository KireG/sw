<?php

namespace KG\Models;

use KG\Models\Product;
use KG\Models\Load;

class Book extends Product implements Load_interface , Loger_interface{
     
    public function query(){
        return $sql= "SELECT books.id,books.name,books.SKU,books.price,books.type_id,types.name as type,books.weight
        FROM books JOIN types ON books.type_id=types.id             
        ORDER BY books.name";    
    } 

    public function get_object_data($data){
        
       return  $new_object=[
          'name'=> $_POST['name'],
          'sku'=>$_POST['SKU'],
          'price'=>$_POST['price'],
          'type_id'=>$_POST['type_id'],
          'weight'=>$_POST['weight'],
      ];
   
    }
     public function gettable(){
        return $table="books";    
     }
  
     public function locationAfterAdd(){
        return $location="productlist";   
   }

}