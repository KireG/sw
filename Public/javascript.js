$(document).ready(function () {
   //Type buton for showing diferent inputs regarding of type of product
    $('#valsel').change( function(){
      var value =$(this).val();
      $('.adddiv').css('display','none');
      $('#mb').add('#weight').add('#height').add('#width').add('#length').removeClass();
      $('#adddiv'+value).css('display','block');
      $('#adddiv'+value+'input').addClass("forminp").addClass("fordim");
      if(value == 1){
        $('#newel').attr("action",'adddvd.php');
      }else if(value == 2){
        $('#newel').attr("action",'addbook.php');
      }else if(value == 3){
        $('#newel').attr("action",'addfurniture.php');
      }else {
        return 'error';
      }
        
    });
    
    //Checkbox button checked attribute
    $(".form-check-input").click( function(){
        if($(this).prop("checked") == true){
          $(this).attr("checked","checked");
        }else if($(this).prop("checked") == false){
          $(this).removeAttr("checked","checked");
        }
    });
  
    //Mass Delete button function
    $("#delbtn").click(function(e){
      e.preventDefault();
      var delddv=[];
      var delbook=[];
      var delfur=[];
      $(".form-check-input").each(function(){
      if($(this).attr("checked")){
        if($(this).data("column") == 1){
          delddv.push($(this).val());
        }if($(this).data("column") == 2){
          delbook.push($(this).val());
        }if($(this).data("column") == 3){
          delfur.push($(this).val());
        }
        
      }  
    });
    window.location.replace("deleteproduct.php?delddv="+ delddv+"&delbook="+ delbook+"&delfur="+ delfur);
    
    })
    
    // Validation if some input fields are empty or wrong format input
    $("#new_product").click(function(e){

      $.each($(".forminp"),function(){

       if($(this).val().length==0 || $("#valsel").val() === " "){
        e.preventDefault();
        $("#openp").css("display","block");
        $("#openn").css("display","none");
       }else if($.isNumeric($("#name").val()) || $.isNumeric($("#sku").val())){
        e.preventDefault();
        $("#openn").css("display","block");
        $("#openp").css("display","none");
       }else if(!$.isNumeric($("#price").val())) {
        e.preventDefault();
        $("#openn").css("display","block");
        $("#openp").css("display","none");
       }else{
        $.each($(".fordim"),function(){
          if(!$.isNumeric($(this).val()) ){ 
           e.preventDefault();
           $("#openn").css("display","block");
           $("#openp").css("display","none");
          }
         });
       }
       
      });
      
    });
       
  //Function for closing pop ups
      $(".close").click(function(){
         $("#openp").css("display","none");
         $("#openn").css("display","none");
      })
  });
  